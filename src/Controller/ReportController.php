<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\ReadStatus;
use App\Entity\Report;
use App\Repository\ReportRepository;
use App\Repository\ReportStatusRepository;
use App\Repository\UnitRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\Persistence\ManagerRegistry;
use http\Client\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    private ReportRepository $reportRepository;
    private UnitRepository $unitRepository;
    private ReportStatusRepository $reportStatusRepository;

    public function __construct(
        ReportRepository $reportRepository,
        UnitRepository $unitRepository,
        ReportStatusRepository $reportStatusRepository
    ) {
        $this->reportRepository       = $reportRepository;
        $this->unitRepository         = $unitRepository;
        $this->reportStatusRepository = $reportStatusRepository;
    }

    public function GetComments($allComments): array
    {
        $comments = [];
        for ($j = 0; $j < count($allComments); $j++) {
            $comment    = $allComments[$j];
            $data       = [
                'content' => $comment->getContent(),
                'user'    => $comment->getUser(),
            ];
            $comments[] = $data;
        }

        return $comments;
    }

    #[Route('/reports', name: 'app_reports', methods: ['GET'])]
    public function index(): JsonResponse
    {
        try {
            $allUnits = $this->unitRepository->findAll();
//            $allUnits = $this->unitRepository->findBy(1);
            $units = [];
            for ($i = 0; $i < count($allUnits); $i++) {
                $unit    = $allUnits[$i];
                $data    = [
                    'id'    => $unit->getId(),
                    'title' => $unit->getTitle(),
                ];
                $units[] = $data;
            }

            $allStatutes    = $this->reportStatusRepository->findAll();
            $reportStatutes = [];
            for ($i = 0; $i < count($allStatutes); $i++) {
                $status           = $allStatutes[$i];
                $data             = [
                    'id'     => $status->getId(),
                    'status' => $status->getStatus(),
                ];
                $reportStatutes[] = $data;
            }

            $allReports = $this->reportRepository->findAll();
//            $allReports = $this->reportRepository->findBy(1);
            $reports = [];
            for ($i = 0; $i < count($allReports); $i++) {
                $report    = $allReports[$i];
                $data      = [
                    'id'              => $report->getId(),
                    'date'            => $report->getDate(),
                    'topic'           => $report->getTopic(),
                    'comment'         => $report->getComment(),
                    'unit'            => $report->getUnit()?->getTitle(),
                    'email'           => $report->getEmail(),
                    'phone'           => $report->getPhone(),
                    'emailPermission' => $report->isEmailPermission(),
                    'phonePermission' => $report->isPhonePermission(),
                    'userAgent'       => $report->getUserAgent(),
                    'isRead'          => $report->getReadStatus()->isIsRead(),
                    'firstReader'     => $report->getReadStatus()
                        ->getFirstReader(),
                    'firstDate'       => $report->getReadStatus()->getFirstDate(
                    ),
                    'status'          => $report->getReportStatus()->getId(),
                    'comments'        => $this->GetComments(
                        $report->getComments()
                    ),
                ];
                $reports[] = $data;
            }

            $data = [
                'reports'        => $reports,
                'units'          => $units,
                'reportStatutes' => $reportStatutes,
            ];
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($data);
    }

    #[Route('/newReport', name: 'app_newReport', methods: ['POST'])]
    public function newReport(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            if ($content['unit'] != null) {
                $unit = $this->unitRepository->find($content['unit']);
            } else {
                $unit = null;
            }

            $reportStatus = $this->reportStatusRepository->findOneBy(
                ['status' => 'nowe']
            );

            $readStatus = new ReadStatus();
            $readStatus->setIsRead(false);
            $entityManager->persist($readStatus);

            $date = new \DateTime();
            $date->format('d-m-Y');

            $report = new Report();
//            $report->setTopic(123);
            $report->setTopic($content['topic']);
            $report->setComment($content['comment']);
            $report->setUnit($unit);
            $report->setEmail($content['email']);
            $report->setPhone($content['phone']);
            $report->setEmailPermission($content['emailPermission']);
            $report->setPhonePermission($content['phonePermission']);
            $report->setDate($date);
            $report->setUserAgent($content['userAgent']);
            $report->setReportStatus($reportStatus);
            $report->setReadStatus($readStatus);
            $entityManager->persist($report);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }

    #[Route('/firstRead', name: 'app_newRead', methods: ['POST'])]
    public function newRead(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $date = new \DateTime();
            $date->format('d-m-Y');

            $report = $this->reportRepository->findOneBy(
                ['id' => $content['id']]
            );

            $readStatus = $report->getReadStatus();

            $readStatus->setIsRead(true);
            $readStatus->setFirstDate($date);
            $readStatus->setFirstReader($content['user']);
            $entityManager->persist($readStatus);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($date);
    }

    #[Route('/setStatus', name: 'app_setStatus', methods: ['POST'])]
    public function newStatus(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $status = $this->reportStatusRepository->findOneBy(
                ['id' => $content['statusId']]
            );
            $report = $this->reportRepository->findOneBy(
                ['id' => $content['reportId']]
            );

            $report->setReportStatus($status);
            $entityManager->persist($report);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }

    #[Route('/newComment', name: 'app_newComment', methods: ['POST'])]
    public function newComment(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $report = $this->reportRepository->findOneBy(
                ['id' => $content['reportId']]
            );

            $comment = new Comment();
            $comment->setContent($content['content']);
            $comment->setUser($content['user']);
            $comment->setReport($report);
            $entityManager->persist($comment);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }


    #[Route('/uploadImage', name: 'app_image', methods: ['POST'])]
    public function uploadImage(
        ManagerRegistry $doctrine,
        Request $request
    ): Response {
        try {
            $entityManager = $doctrine->getManager();

            $content = $request->getContent();
            $image   = base64_decode($content);

            $tempFile     = tmpfile();
            $tempFilePath = stream_get_meta_data($tempFile)['uri'];

            file_put_contents($tempFilePath, $image);

            $tempFileObject = new File($tempFilePath);
//            $file = new UploadedFile(
//                $tempFileObject->getPathname(),
//                $tempFileObject->getFilename(),
//                $tempFileObject->getMimeType(),
//            );

//            $uploadedFile->move("../public/images/image2.jpg");

//            file_put_contents("%kernel.project_dir%/public/images/image2.jpg", $image);

//            $this->upload($image);
//            $file = UPLOAD_DIR . uniqid() . '.jpg';
//            $file = "%kernel.project_dir%/public/images/image2.jpg";
//            file_put_contents($file, $image);

//            $file = fopen("%kernel.project_dir%/public/images/image2.jpg", 'wb');
//            fwrite($file, $image);
//            fclose($file);

//
//            $fs = new Filesystem();
//            $fs->dumpFile('../public/images/image2.jpg', $image);
            $image2 = base64_encode($image);

//            $newImage = new Image();
//            $newImage->setPath();
//            $newImage->setReportId();
//            $entityManager->persist($newImage);
//
//            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->$ex;
        }

        return $this->$image2;
    }
}
