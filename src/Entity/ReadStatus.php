<?php

namespace App\Entity;

use App\Repository\ReadStatusRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReadStatusRepository::class)]
class ReadStatus
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $isRead = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $firstReader = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $firstDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsRead(): ?bool
    {
        return $this->isRead;
    }

    public function setIsRead(bool $isRead): static
    {
        $this->isRead = $isRead;

        return $this;
    }

    public function getFirstReader(): ?string
    {
        return $this->firstReader;
    }

    public function setFirstReader(string $firstReader): static
    {
        $this->firstReader = $firstReader;

        return $this;
    }

    public function getFirstDate(): ?\DateTimeInterface
    {
        return $this->firstDate;
    }

    public function setFirstDate(?\DateTimeInterface $firstDate): static
    {
        $this->firstDate = $firstDate;

        return $this;
    }

}
