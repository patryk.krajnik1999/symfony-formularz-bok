<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\ReadStatus;
use App\Entity\Report;
use App\Entity\ReportStatus;
use App\Entity\Unit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $unit = new Unit();
        $unit->setTitle('Jednostka organizacyjna A');
        $manager->persist($unit);

        $unit2 = new Unit();
        $unit2->setTitle('Jednostka organizacyjna B');
        $manager->persist($unit2);

        $unit3 = new Unit();
        $unit3->setTitle('Jednostka organizacyjna C');
        $manager->persist($unit3);


        $reportStatus = new ReportStatus();
        $reportStatus->setStatus('nowe');
        $manager->persist($reportStatus);

        $reportStatus2 = new ReportStatus();
        $reportStatus2->setStatus('w trakcie realizacji');
        $manager->persist($reportStatus2);

        $reportStatus3 = new ReportStatus();
        $reportStatus3->setStatus('odrzucone');
        $manager->persist($reportStatus3);

        $reportStatus4 = new ReportStatus();
        $reportStatus4->setStatus('zrealizowane');
        $manager->persist($reportStatus4);

        $reportStatus5 = new ReportStatus();
        $reportStatus5->setStatus('duplikat');
        $manager->persist($reportStatus5);

        $readStatus = new ReadStatus();
        $readStatus->setIsRead(false);
        $manager->persist($readStatus);

        $readStatus2 = new ReadStatus();
        $readStatus2->setIsRead(false);
        $manager->persist($readStatus2);


        $report = new Report();
        $date = new \DateTime();
        $date->format('d-m-Y');
        $report->setDate($date);
        $report->setTopic('Niedostępny towar');
        $report->setComment('Na stronie sklepu, że produkt jest dostępny w wybranym salonie. Na miejscu okazało się, że produktu nie me i nigdy nie było.');
        $report->setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36');
        $report->setUnit($unit);
        $report->setEmail('test@test.com');
        $report->setPhone('123123123');
        $report->setEmailPermission(true);
        $report->setPhonePermission(true);
        $report->setReadStatus($readStatus);
        $report->setReportStatus($reportStatus);
        $manager->persist($report);

        $report2 = new Report();
        $date = new \DateTime();
        $date->format('d-m-Y');
        $report2->setDate($date);
        $report2->setTopic('W sklepie za słabo eksponowane są skarpetki z kotami.');
        $report2->setComment('Sugeruję zmianę w ekspozycji za pomocą formularza.');
        $report2->setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36');
        $report2->setReadStatus($readStatus2);
        $report2->setReportStatus($reportStatus);
        $manager->persist($report2);

        $comment = new Comment();
        $comment->setContent('Towar będzie dostępny w najbliższym czasie. Proszę o cierpliwość.');
        $comment->setUser('Użytkownik 11');
        $comment->setReport($report);
        $manager->persist($comment);

        $manager->flush();
    }
}
