<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231009081757 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE report ADD unit_id INT DEFAULT NULL, ADD read_status_id INT NOT NULL, ADD report_status_id INT NOT NULL');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77845F7D6B69 FOREIGN KEY (read_status_id) REFERENCES read_status (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778445436DA5 FOREIGN KEY (report_status_id) REFERENCES report_status (id)');
        $this->addSql('CREATE INDEX IDX_C42F7784F8BD700D ON report (unit_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C42F77845F7D6B69 ON report (read_status_id)');
        $this->addSql('CREATE INDEX IDX_C42F778445436DA5 ON report (report_status_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784F8BD700D');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F77845F7D6B69');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F778445436DA5');
        $this->addSql('DROP INDEX IDX_C42F7784F8BD700D ON report');
        $this->addSql('DROP INDEX UNIQ_C42F77845F7D6B69 ON report');
        $this->addSql('DROP INDEX IDX_C42F778445436DA5 ON report');
        $this->addSql('ALTER TABLE report DROP unit_id, DROP read_status_id, DROP report_status_id');
    }
}
